#!/usr/bin/perl -w

# Reads in a graph file, outputs 2 files .vcolor and .ecolor with the nodes colored according to degree.

####
# Format of graph file:
# (? :) ??? )
#
# NODE
# (adjacent_node-1)
# (adjacent_node-2)
# ...
# (adjacent_node-n)
###

print "SURGERY\n";

$OUTNAME=$ARGV[0] || "SURGERY";

$PI = 3.14159;

$maxdeg=0;

while (<>) {
    if ( /\#(.*)$/ ) {
	$node = $1;
	$DEGREE{$node}++;
    } elsif ( /^(\S+)$/ ) {
	$EDGES{$node}{$1}++;
	$DEGREE{$1}++;
    }
}


foreach $a (keys %EDGES) {
    foreach $b (keys %{$EDGES{$a}}) {

	foreach $i ($a, $b) {
	    if ($DEGREE{$i} > $maxdeg) { $maxdeg = $DEGREE{$i}; }
	}

    }
}

open OUT, ">$OUTNAME.ecolor" || die "no open $OUTNAME.ecolor";

foreach $a (keys %EDGES) {
    foreach $b (keys %{$EDGES{$a}}) {

	$deg = $DEGREE{$a} + $DEGREE{$b};
	
	print OUT "$a $b ", colorize ($deg/2), "\n";
    }
}

close OUT;


open OUT, ">$OUTNAME.vcolor" || die "no open $OUTNAME.vcolor";

foreach (keys %DEGREE) {
    
    print OUT "$_ ", colorize ($DEGREE{$_}), "\n";

}

close OUT;


sub colorize {
    my $deg = shift() / $maxdeg;

    $RED = sin($deg * $PI/2) ;

    $GREEN = sin(($deg + .5) * $PI/2);

    $BLUE = sin(($deg + 1) * $PI/2);
    
    sprintf ("%f %f %f", $RED, $GREEN, $BLUE);
    
}
